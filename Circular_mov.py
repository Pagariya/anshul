import pybullet as p
import pybullet_data
import math
import numpy as np
import matplotlib.pyplot as plt

physicsClient = p.connect(p.GUI)  # or p.DIRECT for non-graphical version
p.setAdditionalSearchPath(pybullet_data.getDataPath())  # optionally
p.setGravity(0, 0, -10)  # describe gravity in (x,y,z)
planeId = p.loadURDF("plane.urdf")
base_Position = [0, 0, .3]
UR5Id = p.loadURDF(
    "/home/anshul/Documents/pybulletproject/robot_movement_interface/dependencies/ur_description/urdf/ur5_robot.urdf",
    useFixedBase=1, basePosition=base_Position)

starting_point = [0.3, 0.3, 0.5]
target_ori_euler = [-math.pi / 2, -math.pi / 2, 0]
target_ori_quaternion = p.getQuaternionFromEuler(target_ori_euler)

num_samples = 20

# make a simple unit circle
theta = np.linspace(0, 2 * np.pi, num_samples)
x, y = starting_point[0] + 0.2 * np.cos(theta), starting_point[1] + 0.2 * np.sin(theta)
link_states = []

for i in range(20):
    p.resetDebugVisualizerCamera(cameraDistance=3, cameraYaw=217.60, cameraPitch=-99.80,
                                 cameraTargetPosition=[-0.03, -0.04, -0.30])
    target_joint = p.calculateInverseKinematics(UR5Id, 7, targetPosition=[x[i], y[i], 0.5],
                                                targetOrientation=target_ori_quaternion)
    p.setJointMotorControlArray(UR5Id, range(1, 7), p.POSITION_CONTROL, targetPositions=target_joint)
    for _ in range(300):
        p.stepSimulation()
    link_states.append(list(p.getLinkState(UR5Id, 7)[0]))

# print(link_states)
#
# print(link_states[:][1])
#
linkstate_array = np.array(link_states)
# print(linkstate_array)
# print(linkstate_array[:,0])
plt.plot(x, y, 'b')
plt.plot(linkstate_array[:, 0], linkstate_array[:, 1], "r")
plt.title("plot showing the path followed by UR5, Red =Robot Path Blue= Our Input")
plt.xlabel("x-cordinates")
plt.ylabel("y-cordinates")
# plt.text(3, 2, "Red line = Robot Path, Blue line = Our input of circle points")
plt.show()

